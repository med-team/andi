#!/bin/sh -f

# copied and enhanced from upstream

set -e
pkg=andi

export LC_ALL=C.UTF-8
if [ "${AUTOPKGTEST_TMP}" = "" ] ; then
  AUTOPKGTEST_TMP=$(mktemp -d /tmp/${pkg}-test.XXXXXX)
  # Double quote below to expand the temporary directory variable now versus
  # later is on purpose.
  # shellcheck disable=SC2064
  trap "rm -rf ${AUTOPKGTEST_TMP}" 0 INT QUIT ABRT PIPE TERM
fi

cp test/test_fasta.cxx "${AUTOPKGTEST_TMP}"

cd "${AUTOPKGTEST_TMP}"

g++ -O2 -Wall -o test_fasta test_fasta.cxx
RANDOM_SEED=1729



#--- only small changes below ---

SEED=${RANDOM_SEED:-0}
SEED2=0
SEED3=0
if test $SEED -ne 0; then
	SEED=$((SEED + 1))
	SEED2=$((SEED + 2))
	SEED3=$((SEED + 3))
fi

./test_fasta -s $SEED -l 100000 > a_low.fa
./test_fasta -s $SEED2 -l 100000 > b_low.fa
./test_fasta -s $SEED3 -l 100 > both_low.fa

cat both_low.fa a_low.fa | awk -v RS='>' '{if($1 == "S0")print ">"$0 > "S0_low.fa"}'
cat both_low.fa b_low.fa | awk -v RS='>' '{if($1 == "S1")print ">"$0 > "S1_low.fa"}'

# this is expected to trigger the low homology warning
andi -j S0_low.fa S1_low.fa 2>&1 | grep 'homology'
EXIT_VAL=$?

if [ ${EXIT_VAL} -ge 1 ]; then
	echo "Triggering low homology failed" >&2
	grep '^>' a_low.fa b_low.fa both_low.fa
fi

rm -f a_low.fa b_low.fa both_low.fa S0_low.fa S1_low.fa
exit $EXIT_VAL
