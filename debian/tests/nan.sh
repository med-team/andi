#!/bin/sh -f

# copied and enhanced from upstream

set -e
pkg=andi

export LC_ALL=C.UTF-8
if [ "${AUTOPKGTEST_TMP}" = "" ] ; then
  AUTOPKGTEST_TMP=$(mktemp -d /tmp/${pkg}-test.XXXXXX)
  # Double quote below to expand the temporary directory variable now versus
  # later is on purpose.
  # shellcheck disable=SC2064
  trap "rm -rf ${AUTOPKGTEST_TMP}" 0 INT QUIT ABRT PIPE TERM
fi

cp test/test_fasta.cxx "${AUTOPKGTEST_TMP}"

cd "${AUTOPKGTEST_TMP}"

g++ -O2 -Wall -o test_fasta test_fasta.cxx
RANDOM_SEED=1729



#--- only small changes below ---

SEED=${RANDOM_SEED:-0}
SEED2=0
if test $SEED -ne 0; then
	SEED=$((SEED + 1))
	SEED2=$((SEED + 2))
fi


./test_fasta -s $SEED -l 10000 > a_nan.fa
./test_fasta -s $SEED2 -l 10000 > b_nan.fa

# this is expected to trigger the nan warning
andi -j a_nan.fa b_nan.fa 2>&1 | grep 'nan'
EXIT_VAL=$?


if [ $EXIT_VAL -ge 1 ]; then
	echo "Triggering nan failed" >&2
	grep '^>' a_nan.fa b_nan.fa
fi

rm -f a_nan.fa b_nan.fa
exit $EXIT_VAL
