#!/bin/sh -f

# copied and enhanced from upstream

set -e
pkg=andi

export LC_ALL=C.UTF-8
if [ "${AUTOPKGTEST_TMP}" = "" ] ; then
  AUTOPKGTEST_TMP=$(mktemp -d /tmp/${pkg}-test.XXXXXX)
  # Double quote below to expand the temporary directory variable now versus
  # later is on purpose.
  # shellcheck disable=SC2064
  trap "rm -rf ${AUTOPKGTEST_TMP}" 0 INT QUIT ABRT PIPE TERM
fi

cp test/test_fasta.cxx "${AUTOPKGTEST_TMP}"

cd "${AUTOPKGTEST_TMP}"

g++ -O2 -Wall -o test_fasta test_fasta.cxx
RANDOM_SEED=1729



#--- only small changes below ---

# Test if andi exists, and can be executed
andi --version > /dev/null || exit 1

SEED=${RANDOM_SEED:-0}
SEED2=0
SEED3=0
if test $SEED -ne 0; then
        SEED=$((SEED + 1))
        SEED2=$((SEED + 2))
        SEED3=$((SEED + 3))
fi

# Test andi for more than just two sequences at a time
./test_fasta -s $SEED -l 100000 -d 0.01 -d 0.01 -d 0.01 -d 0.01 | andi > /dev/null || exit 1

# Test low-memory mode
./test_fasta -s $SEED2 -l 10000 > test_extra.fasta
andi test_extra.fasta > extra.out
andi test_extra.fasta --low-memory > extra_low_memory.out
diff extra.out extra_low_memory.out || exit 1

# Test file of filenames
./test_fasta -s $SEED3 -l 10000 > test_extra.fasta
echo "$PWD/test_extra.fasta" > fof.txt
andi test_extra.fasta > extra.out
andi --file-of-filenames fof.txt > fof.out
cat fof.txt | andi --file-of-filenames - > fof2.out
diff extra.out fof.out || exit 1
diff extra.out fof2.out || exit 1


rm -f test_extra.fasta extra.out extra_low_memory.out fof.out fof2.out fof.txt

