#!/bin/sh -f

# copied and enhanced from upstream

set -e
pkg=andi

export LC_ALL=C.UTF-8
if [ "${AUTOPKGTEST_TMP}" = "" ] ; then
  AUTOPKGTEST_TMP=$(mktemp -d /tmp/${pkg}-test.XXXXXX)
  # Double quote below to expand the temporary directory variable now versus
  # later is on purpose.
  # shellcheck disable=SC2064
  trap "rm -rf ${AUTOPKGTEST_TMP}" 0 INT QUIT ABRT PIPE TERM
fi

cp test/test_fasta.cxx "${AUTOPKGTEST_TMP}"

cd "${AUTOPKGTEST_TMP}"

g++ -O2 -Wall -o test_fasta test_fasta.cxx
RANDOM_SEED=1729



#--- only small changes below ---

# This scripts test the accuracy of andi with random inputs. For that
# it uses the small program test_random to generate pairs of sequences
# with a given distance. By default, test_random creates a new set of
# sequences each time it is called. Thus, this test has a small, but
# non-zero probability of failing. That is a problem with Debian's
# reproducible builds effort. So this script acts as a wrapper around
# this issue.
#
# Simply calling this script via
#     % ./test/test_random.sh
# checks a new test-case every time. But with the right parameter
#     % RANDOM_SEED=1729 ./test/test_random.sh
# one specific set of sequences is validated.

andi --help > /dev/null || exit 1

LENGTH=100000

# If RANDOM_SEED is set, use its value. Otherwise 0 is used to signal
# to test_random that a new set of sequences shall be generated.
SEED=${RANDOM_SEED:-0}

for dist in 0.0 0.001 0.01 0.02 0.05 0.1 0.2 0.3
do
	for n in $(seq 10)
	do
		if test $SEED -ne 0; then
			SEED=$((SEED + 1))
		fi

		res=$(./test_fasta -s $SEED -l $LENGTH -d $dist |
			tee test_random.fasta |
			andi -t 1 |
			tail -n 1 |
			awk -v dist=$dist '{print $2, dist}' |
			awk 'function abs(x){return ((x < 0.0) ? -x : x)} {print abs($1-$2) <= 0.055 && abs($1-$2) <= 0.055 * $2}')
		if test $res -ne 1; then
			echo "The last test computed a distance deviating more than five percent from its intended value."
			echo "See test_random.fasta for the used sequences."
			echo "./test_fasta -s $SEED -l $LENGTH -d $dist"
			head -n 1 test_random.fasta
			exit 1;
		fi
	done

	# raw
	for n in $(seq 10)
	do
		if test $SEED -ne 0; then
			SEED=$((SEED + 1))
		fi

		res=$(./test_fasta -r -s $SEED -l $LENGTH -d $dist |
			tee test_random.fasta |
			andi -m RAW -t 1 |
			tail -n 1 |
			awk -v dist=$dist '{print $2, dist}' |
			awk 'function abs(x){return ((x < 0.0) ? -x : x)} {print abs($1-$2) <= 0.055 && abs($1-$2) <= 0.055 * $2}')
		if test $res -ne 1; then
			echo "The last test computed a distance deviating more than five percent from its intended value."
			echo "See test_random.fasta for the used sequences."
			echo "./test_fasta -r -s $SEED -l $LENGTH -d $dist"
			head -n 1 test_random.fasta
			exit 1;
		fi
	done
done

rm test_random.fasta
